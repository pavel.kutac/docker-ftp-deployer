.PHONY: set-local-variables push-all build-php83 build-latest-multiplatform

set-local-variables:
	$(eval CI_REGISTRY_IMAGE := $(or $(CI_REGISTRY_IMAGE),'registry.gitlab.com/pavel.kutac/docker-ftp-deployer'))
	$(eval CI_COMMIT_TAG := $(or $(CI_COMMIT_TAG),''))

push-all: set-local-variables
	docker push --all-tags "$(CI_REGISTRY_IMAGE)"

build-php83: set-local-variables
	docker build --pull --build-arg PHP_VERSION=8.3 -t $(CI_REGISTRY_IMAGE):php83 .
	docker tag $(CI_REGISTRY_IMAGE):php83 $(CI_REGISTRY_IMAGE):php83_$(CI_COMMIT_TAG)
	docker image prune --force --filter label=stage=builder

build-latest-multiplatform: set-local-variables
	docker buildx build --pull --push --build-arg PHP_VERSION=8.3 -t $(CI_REGISTRY_IMAGE):latest -t $(CI_REGISTRY_IMAGE):php83 -t $(CI_REGISTRY_IMAGE):php83_$(CI_COMMIT_TAG) --platform linux/amd64,linux/arm .
	docker image prune --force --filter label=stage=builder
