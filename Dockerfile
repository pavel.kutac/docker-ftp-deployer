ARG PHP_VERSION=8.3
FROM php:${PHP_VERSION}-alpine AS prepare
LABEL stage=builder

WORKDIR /var

COPY replaceEnvVars.php phpenvsubst
COPY getNextVersionGitlab.php gitlab-semver-bump

RUN apk update \
    && apk add unzip \
    && wget https://raw.githubusercontent.com/fsaintjacques/semver-tool/3.3.0/src/semver \
    # Add composer
    && wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php'); unlink('installer.sig');" \
    && mv composer.phar composer \
    # Add FTP Deployment
    && wget --output-document=/var/deployer_source.zip https://github.com/arxeiss/ftp-deployment/archive/refs/tags/v3.6.1.1.zip \
    && echo "phar.readonly=Off" > /usr/local/etc/php/php.ini \
    && unzip /var/deployer_source.zip -d /var \
    && php composer install --working-dir=/var/ftp-deployment-3.6.1.1 --optimize-autoloader --ignore-platform-reqs --prefer-dist --no-dev --no-ansi --no-interaction \
    && php /var/ftp-deployment-3.6.1.1/generatePhar --compress \
    && mv /var/ftp-deployment-3.6.1.1/deployment.phar /var/ftp-deployment-3.6.1.1/deployment \
    && chmod +x /var/ftp-deployment-3.6.1.1/deployment phpenvsubst semver gitlab-semver-bump

########################
# Building final image #
########################

FROM php:${PHP_VERSION}-alpine

COPY --from=prepare /var/ftp-deployment-3.6.1.1/deployment \
    /var/composer \
    /var/phpenvsubst \
    /var/semver \
    /var/gitlab-semver-bump \
    /bin/

RUN apk update \
    && apk add openssl-dev make git bash lftp coreutils

RUN docker-php-ext-configure ftp --with-openssl-dir=/usr \
	&& docker-php-ext-install ftp
