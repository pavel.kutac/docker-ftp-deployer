#!/usr/bin/env php
<?php

if (!isset($_SERVER['argv']) || count($_SERVER['argv']) !== 3) {
	echo "Replacing ENV variables in given file in format \$XXX or \${XXX}".PHP_EOL.PHP_EOL;
	echo "  Usage ".basename(__FILE__)." [path_to_source] [path_to_output]".PHP_EOL.PHP_EOL;
	die(0);
}

$source = $_SERVER['argv'][1];
$dest = $_SERVER['argv'][2];

if (!file_exists($source) || !is_readable($source)) {
	echo "The source file $source is not readable".PHP_EOL;
	die(1);
}

$content = file_get_contents($source);
if ($content === false) {
	echo "The source file $source is not readable".PHP_EOL;
	die(1);
}

$env = getenv();
$content = preg_replace_callback('/\$({)?([A-Z0-9_]+)(?(1)}|\b)/', function ($match) use($env) {
	[$search, $x, $variableName] = $match;
	if (isset($env[$variableName])) {
		return $env[$variableName];
	}
	return $search;
}, $content);


if (file_put_contents($dest, $content) === false) {
	echo "The dest file $dest is not writeable".PHP_EOL;
	die(1);
}
