# CHANGELOG

## v3.0

### v3.0 - 31.7.2024
- Build only PHP 8.3 image
- Update FTP Deployment to `3.6.1`

## v2.0

### v2.1 - 6.3.2022
- Add support for ARM

### v2.0 - 5.3.2022
- Change versioning and docker tags to support multiple PHP versions
- Increase FTP Deployment version to `3.5.2`
- Increase semver tool version to `3.3.0`

## v1.0

### v1.3 - 29.8.2021
- Increase FTP Deployment version to `3.5.0`
- Increase semver tool version to `3.2.0`

### v1.2 - 20.4.2021
- Increase FTP Deployment version to `3.4.0.2`

### v1.1 - 16.4.2021
- Increase FTP Deployment version to `3.4.0`

### v1.0 - 15.11.2020
- Initial release
