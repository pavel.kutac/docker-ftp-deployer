# Docker FTP Deployer

Docker image with [LFTP](https://github.com/lavv17/lftp), modified version of [FTP Deployment](https://github.com/arxeiss/ftp-deployment/) and extra  utilities,
which works together to support **incremental and parallel upload to FTP** - upload only changes.

Other included utilities or programs:
- `Composer` - PHP Dependency manager https://getcomposer.org/
- `git` - version control system https://git-scm.com/
- `phpenvsubst` - PHP script to replace environment variables inside any file
- `semver` - The semver shell utility: https://github.com/fsaintjacques/semver-tool
- `gitlab-semver-bump` - PHP script uses `semver` to get next semver tag according to Gitlab Merge Request label
- *Create MR with your own utility you want to include*

## How?

For examples see [example](./example) folder with scripts ready to go. The deployment process is described there with more details. Just keep in mind that the flow is fully customizable with the scripts etc. There is no need to use all utilities. Also, you are free to write your own utility and create MR or keep it just in your repo with the project.

### TL;DR:

**Base Deploy:**

1. FTP Deployment download `.htdeployment` file from FTP, if exists. In that file, the hashes of all files are tracked and then the file saved in FTP alongside the project.
1. FTP Deployment count hashes of all local files and compare them with `.htdeployment` file. Then produce a list of files and directories to upload and delete.
1. LFTP reads those lists and perform required actions.

**Full Deploy:**

1. Same as above
1. FTP Deployment count hashes of all local files and compare them with `.htdeployment` file. But do not produce list of changes, just modify `.htdeployment` file.
1. LFTP performs `mirror` command, which will do 1:1 copy.

> ⚠️ Be careful with **Full deploy** as it can delete other files (like uploads etc.) if they are not excluded properly.

## Why?

1. **Not all public web hostings support composer**, so it is up to developers to use only FTP. Sadly it is still true.
1. **FTP Deployment is slow**, it can upload only 1 file at the same time, so it takes ages to upload bigger changes in vendor folder.
1. **LFTP cannot upload only changes**. Sure, there is a mirror option. But it compares only Modified time, which some FTP servers do not preserve and/or file size.
  - If the comparison is based only on file size, a change in a single character will not trigger upload - not safe in my eyes.
  - If the comparison is based only on Modified time, if the FTP server does not preserver that time, all files are uploaded always.

## Utilities

### Composer - Update, or downgrade to version 1.x

During the Docker image building, newest version of PHP Composer is installed. This means **version 2.x** is already there. But for some older projects **version 1.x** is required. There is a way how to downgrade but always use the latest version of Docker FTP Deployer.

- Run `composer self-update --1` as a first step to **downgrade to version 1** of Composer.
- Run `composer self-update` as a first step to **upgrade to the latests version** of Composer

### gitlab-semver-bump

Utility `gitlab-semver-bump` is written in the PHP and source is in file `getNextVersionGitlab.php`. Get from `git` last tag and parse semantic version which must be in the format `vX.Y.Z` like `v1.2.3` etc. Then grabs ID of the Merge request which was merged, if possible, and gets assigned labels. If label `BUMP_MAJOR` is present, `semver` utility is used to bump the major version from the last tag. The same happens with the `BUMP_MINOR` label if present. If no labels are present, the PATCH version is bumped.

> Label name can be changed with `LABEL_NAME_BUMP_MAJOR` and `LABEL_NAME_BUMP_MINOR` environment variables.

**There is a problem with getting the ID of merged Merge request in Gitlab.** Currently this is not possible at all via `$CI_*` variables. But after a merge, the commit message contains `See merge request user/project!123` where `123` is the ID of MR. This message is parsed and labels are searched within MR with that parsed ID.

### phpenvsubst

Utility `phpenvsubst` is written in PHP and source lives in file `replaceEnvVars.php`. When executing, it is expecting 2 parameters - input and output file. When reading the input file, all occurrences of `$XXX` or `${XXX}` where XXX must be an alphanumerical uppercase name possibly with `_` (underscore), are processed. If there is an environment variable with a given name, the text is replaced, otherwise, the text remains untouched.

- Example input: `Hello $NAME, you can find ${ME} in the ${PLACE}-zone offices`.
- ENV variables: `NAME=visitor` and `PLACE=dark`
- Output: `Hello visitor, you can fine ${ME} in the dark-zone offices`
