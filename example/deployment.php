<?php
// Top level config
$config = [
    'fullCliLog' => ["http", "exception"],
    'log' => __DIR__.'/../output/deployment.log',
];

$sections = require 'deployment.sections.php';

// Fill your own needs, this is example from my personal Laravel project
$ignore = [
    '.git*',
    '.ci',
    '/bootstrap/compiled.php',
    '/bootstrap/cache/*',
    '/database/factories',
    '/public/files/*',
    '/public/uploads/*',
    '/resources/assets/js/*',
    '/resources/assets/sass/*',
    '/resources/views/mail/_sun_templates/*.blade.php',
    '/storage/*.key',
    '/storage/app/public/*',
    '/storage/app/*',
    '/storage/debugbar',
    '/storage/framework/cache/*',
    '/storage/framework/sessions/*',
    '/storage/framework/testing',
    '/storage/framework/views/*',
    '/storage/framework/config.php',
    '/storage/framework/routes.php',
    '/storage/framework/schedule-*',
    '/storage/framework/compiled.php',
    '/storage/framework/services.json',
    '/storage/framework/events.scanned.php',
    '/storage/framework/routes.scanned.php',
    '/storage/framework/down',
    '/storage/logs/*',
    'tests/', // This ignores test folders everywhere, not just top level
    '/node_modules',
    '.env',
    '*.log',
    '.browserslistrc',
    '.editorconfig',
    '.eslintignore',
    '.eslintrc.js',
    '.htaccess',
    '.prettierignore',
    '.prettierrc',
    '.stylelintignore',
    '.stylelintrc',
    'artisan',
    'composer.phar',
    'Makefile',
    'package.json',
    'phpcs.xml',
    'phpcs_lang.xml',
    'phpstan.neon',
    'phpunit.xml',
    'postcss.config.js',
    'README.md',
    'renovate.json',
    '/server.php', // Ignore server.php file only in root, not inside nested folders
    '*.sublime-*',
    'tsconfig.json',
    'webpack.config.js',
];

foreach ($sections as $name => $settings) {
    $config[$name] = array_merge($settings, [
        'ignore' => $ignore,
        'local' => '../../',
        'fileOutputDir' => '../output/',
    ]);
}

return $config;
