<?php

// Set as many sections as needed, in CI is section specified
// All variables like ${xxx} are automatically replaced with phpenvsubst utility in CI from environment variables
return [
    'live' => [
        'remote'        => "ftp://${FTP_SERVER}/${FTP_DIRECTORY}",
        'user'          => "${FTP_USERNAME}",
        'password'      => "${SERVER_FTP_PASSWORD}",
    ],
];
