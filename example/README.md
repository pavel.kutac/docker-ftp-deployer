# Example usage

I suggest creating the following directory structure inside your **project root** folder. The example `.gitlab-ci.yml` file expects it.

```
.ci
 |- deploy
 |	|- deployment.php
 |	|- deployment.sections.sample.php
 |	|- lftp-ignore.rx
 |	|- run-lftp-full-mirror.sh
 |	|- run-lftp-incremental-sync.sh
 |	|- ... .env.example, .htaccess.example etc. Files need to be modified before upload - example scripts except ".env.example" and ".htaccess.example"
 |
 |- output
 	|- .gitignore
```

## Used environment variables

- `GITLAB_API_AUTH_TOKEN` - Used for pushing git tags to the repository after a successful deploy
- `SERVER_FTP_PASSWORD` - Used in `.gitlab-ci.yml` file

## How it works

**Base Deploy:**

1. FTP Deployment download `.htdeployment` file from FTP, if exists. In that file, the hashes of all files are tracked, and then the file saved in FTP alongside the project.
1. FTP Deployment count hashes of all local files and compare them with `.htdeployment` file. Then produce a list of files and directories to upload and delete.
1. LFTP reads those lists and perform required actions.

**Full Deploy:**

1. Same as above
1. FTP Deployment count hashes of all local files and compare them with `.htdeployment` file. But does not produce a list of changes, just modify `.htdeployment` file.
1. LFTP performs `mirror` command, which will do 1:1 copy. See [manual](http://lftp.yar.ru/lftp-man.html)

> ⚠️ Be careful with **Full deploy** as it can delete other files (like uploads etc.) if they are not excluded properly.

## About files

#### Entrypoint: .gitlab-ci.yml:

- Using `.ci/output/` directory as artifacts, outputs from FTP and LFTP are saved there for easier debugging
- Both steps `deploy` and `fullDeploy` extends `.baseDeploy` stage. Stages starting with `.` are not executed
- Installing composer dependencies, replacing all environment variables defined either globally or in `.gitlab-ci.yml` file with `phpenvsubst` utility.
- Running FTP Deployment for generate `.htdeployment` file and generate a list of changed/removed files/folders.
- Using `gitlab-semver-bump` to get next semver tag - see more in [#gitlab-semver-bump section](../README.md#gitlab-semver-bump).
- Uploading files with LFTP - incremental sync or full mirror
- After a successful deploy, creates tag from `gitlab-semver-bump` in the repository directly via API call.

#### deployment.php and deployment.sections.sample.php:

- In `.gitlab-ci.yml` the content of the file `deployment.sections.sample.php` is taken and all environment variables are expanded. The output is saved into `deployment.sections.php`
- FTP Deployment takes `deployment.php` as the config, which then requires `deployment.sections.php` which contains sensitive data, like FTP password.
- Sets the `.ci/output` directory as the output for the files containing a list of files/folders to create/upload/delete.

#### run-lftp-incremental-sync.sh

- Script which is reading output files from FTP Deployment
- Files to upload are split into chunks of 500 entries - easier to print progress more often
- It builds `.ci/output/lftp_commands.sh` script, which is then executed directly by LFTP. The file is also saved as an artifact for easier debugging

#### run-lftp-full-mirror.sh

- Script performing `mirror` [command of LFTP](http://lftp.yar.ru/lftp-man.html)
- Saves also debug log into `.ci/output` folder.

#### lftp-ignore.rx

- Contains files to ignore when performing `mirror` command - used in **Full Deploy**.
- Should be in sync with `deployment.php` but contains little different syntax


## Content of `.gitignore` files

**`.ci/deploy/.gitignore`**:

```gitignore
deployment.sections.php
.env
.htaccess
```

**`.ci/output/.gitignore`**:

```gitignore
*
!.gitignore
```
