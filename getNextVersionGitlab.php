#!/usr/bin/env php
<?php

function apiCall($env, $url, $method = 'GET')
{
	$context = [
		'http' => [
			'method' => $method,
			'header' => 'PRIVATE-TOKEN: '.$env['GITLAB_API_AUTH_TOKEN']."\r\n"
		]
	];

	return file_get_contents(
		sprintf('%s/projects/%s/%s', $env['CI_API_V4_URL'], $env['CI_PROJECT_ID'], $url),
		false,
		stream_context_create($context)
	);
}

function exitErr($text, $exitCode = 1)
{
	fwrite(STDERR, $text);
    exit($exitCode);
}

$env = getenv();
$tag = @exec('git describe --tags');
$version = "0.0.0";
if (preg_match('/^v([0-9]+\.[0-9]+\.[0-9]+)/', $tag, $matches)) {
    $version = $matches[1];
}else {
	exitErr("The latest tag '$tag' does not match version pattern.");
}

$bump = 'patch';
$pattern = preg_quote(sprintf('See merge request %s!', $env['CI_PROJECT_PATH']), '/');
if (preg_match('/'.$pattern.'([0-9]+)/', $env['CI_COMMIT_MESSAGE'], $mrMatches)) {
	$mr = apiCall($env, 'merge_requests/'.$mrMatches[1]);
	if (!$mr) {
		exitErr("Merge request $mr was not found\nmerge_requests/".$mrMatches[1]);
	}
	$mr = json_decode($mr);
	if (in_array($env['LABEL_NAME_BUMP_MAJOR'] ?? "BUMP_MAJOR", $mr->labels)) {
		$bump = 'major';
	}elseif(in_array($env['LABEL_NAME_BUMP_MINOR'] ?? "BUMP_MINOR", $mr->labels)){
		$bump = 'minor';
	}
}

echo "v".exec(sprintf('semver bump %s %s', $bump, $version));
